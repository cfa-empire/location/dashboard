import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
	//path: '/verification/:type',
	//login
	{
		path: '/login',
		name: 'login',
		component: () =>
			import('../views/Login.vue')
	},
	//register
	{
		path: '/register',
		name: 'register',
		component: () =>
			import('../views/Register.vue')
	},
	//accueil
	{
		path: '/accueil',
		name: 'Home',
		component: () =>
			import('../views/Home.vue'),
		meta: {
			requiresAuth: false
		}
	},
	//locations
	{
		path: '/locations',
		name: 'locations',
		component: () =>
			import('../views/Locations.vue'),
		meta: {
			requiresAuth: true
		}
	},
	//proprietaires
	{
		path: '/proprietaires',
		name: 'proprietaires',
		component: () =>
			import('../views/Proprietaires.vue'),
		meta: {
			requiresAuth: true
		}
	},
	//agence update
	{
		path: '/agenceupdate',
		name: 'agenceupdate',
		component: () =>
			import('../views/AgenceUpdate.vue'),
		meta: {
			requiresAuth: true
		}
	},
	//historiques
	{
		path: '/historiques',
		name: 'historiques',
		component: () =>
			import('../views/Historiques.vue'),
		meta: {
			requiresAuth: true
		}
	},
	//users
	{
		path: '/users',
		name: 'users',
		component: () =>
			import('../views/users/Users.vue'),
		meta: {
			requiresAuth: true
		},
		props: true,
		children: [
			{
				path: 'agence',
				component: () =>
					import('../views/users/UsersAgence.vue'),
			},
			{
				path: 'client',
				component: () =>
					import('../views/users/UsersClient.vue'),
			},
			{
				path: 'admin',
				component: () =>
					import('../views/users/UsersAdmin.vue'),
			},
			{
				path: 'detail/:adminId',
				component: () =>
					import('../views/users/UsersDetail.vue'),
			}
		]
	},
	/*
	{
		path: '/admins',
		name: 'admins',
		component: () =>
			import('../views/Admin.vue'),
		meta: {
			requiresAuth: true
		}
	},*/
	// Routage links Web site
	{
		path: '/em',
		name: 'devenir-membre',
		component: () =>
			import('../views/EtreMembre.vue'),
		meta: {
			requiresAuth: false
		}
	},
	{
		path: '/about',
		name: 'about',
		component: () =>
			import('../views/About.vue'),
		meta: {
			requiresAuth: false
		}
	},

]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
});

router.beforeEach((to, from, next) => {

	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (!localStorage.getItem('token')) {
			next({ path: '/' })
		} else {
			if (to.path == "/") {
				next({ path: '/accueil' });
			} else {
				next();
			}
		}
	} else {
		if (to.path == "/") {
			next({ path: '/accueil' });
		} else {
			next();
		}
	}
})

export default router