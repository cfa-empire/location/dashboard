export default {
	admin: [
		/*
		{
		title: "Agences",
		path: "/agences",
		icon: "building",
		},*/
		{
			title: "Propriétaires",
			path: "/proprietaires",
			icon: "users",
		},
		{
			title: "Locations",
			path: "/locations",
			icon: "home",
		},
		{
			title: "Utilisateurs",
			path: "/users/agence",
			icon: "user",
		},
		{
			title: "Historiques",
			path: "/historiques",
			icon: "book",
		},
	],

	agence: [
		{
			title: "Propriétaires",
			path: "/proprietaires",
			icon: "users",
		},
		{
			title: "Locations",
			path: "/locations",
			icon: "home",
		},
		{
			title: "A propos",
			path: "/about",
			icon: "info",
		},
	],
}